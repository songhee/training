# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Java
eclipes

### Contribution guidelines ###

### Who do I talk to? ###

I made this project to study Java. 
this is very important Java skill like Generic/collectionframework... so on.. 

Generic 
→ GenericBaseFruitBox.java
   GenericDemo.java 
   ObjectBaseFrutBox.java 

ObjectClone
→ InstanceCloning.java 
   ShallowCopy.java 

ObjectEquality
→ EncapsulationEquals.java 
   ObjectEquality.java    
   ObjectEquality2.java 

Singleton
→ Main.java 
   Singleton.java 

Thread
→ PriorityTestOne.java 
   PriorityTestThread.java 
   PriorityTestTwo.java 
   RunnableThread.java 
   Test49.java 
   ThreadSyncError.java 
   ThreadUnderstand.java
 
Wrapper
→ AutoBoxingUnboxing.java 
   BoxingUnboxing.java 
   UseWrapperClass.java 
   WrappingInteger.java
 
callback1
→ Bulldog.java 
   Dog.java 
   DogSound.java 
   Main.java 
   Martis.java 
   Poodle.java 

callback2
→ Accountant.java 
   Company.java 
   Developer.java 
   Employee.java 
   Main.java 
   Salesman.java 

callback3
→ Main.java

collectionsframework
→ IntroArrayList.java 
   IntroHashMap.java 
   IntroLinkedList.java 
   IteratorUsage.java 
   MapDemo.java 
   PrimitiveCollection.java 
   SetDemo.java 
   SimpleNumber.java 
   UserfulIterator.java