package Generic;

class Orange_ {
	
	int sugarContent;
	
	public Orange_(int sugar) {sugarContent=sugar;}
	public void showSugarContent() {System.out.println("당도 : "+sugarContent);}
}

class Apple_  {
	int weight;
	public Apple_(int weight) { this.weight = weight;}
	public void showAppleWeight() {
		System.out.println("무게 : "+weight);
	}
}

class FruitBox_<T> {
	T item; 
	public void store(T item) {this.item = item;}
	public T pullOut() {return item;}
}

public class GenericBaseFruitBox {

	public static void main (String[] args) {
		FruitBox_<Orange_> orBox = new FruitBox_<Orange_>();
		orBox.store(new Orange_(10));
		Orange_ org = orBox.pullOut();
		org.showSugarContent();
		
		FruitBox_<Apple_> apBox = new FruitBox_<Apple_>();
		apBox.store(new Apple_(10));
		Apple_ app = apBox.pullOut();
		app.showAppleWeight();
			
	}
}
