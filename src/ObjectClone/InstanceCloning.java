package ObjectClone;


class Point implements Cloneable {
	
	private int xPos;
	private int yPos;
	
	public Point(int x, int y) {
		
		xPos = x;
		yPos = y;
	}
	
	public void showPoint() {
		System.out.printf("[%d, %d]", xPos, yPos);
		System.out.println("");
	}
	
	// clone메소드 오버라이드 
	@Override
	protected Object clone() throws CloneNotSupportedException {

		return super.clone();
	}
}

public class InstanceCloning {
	
	public static void main(String[] args) {
		
		Point org = new Point(3,5);
		Point cpy;
		
		// try, catch 가 있으므로 main에 throw CloneNotSupportedException 을 적어줄 필요가 없다. 
		try {
			cpy = (Point)org.clone();
			org.showPoint();
			cpy.showPoint();
			
		} catch (CloneNotSupportedException e) {
			
			e.printStackTrace();  // getMessage() 호출해서 반환된 문자열을 출력해줌 
		}
	}
	
}
