package ObjectClone;


class Point1 implements Cloneable {
	
	// DeepCopy와 ShallowCopy 동일
	private int xPos;
	private int yPos; 
	
	public Point1 (int x, int y) {
		
		xPos = x;
		yPos = y;
	}
	

	public void showPosition() {
		
		System.out.printf("[%d, %d]", xPos, yPos);
	}
	
	public void changePos(int x, int y) {
		
		xPos = x; 
		yPos = y;
	}
	
	// clone메소드 오버라이드 
	@Override
	protected Object clone() throws CloneNotSupportedException {

		return super.clone();
	}
	
}



class Rectangle implements Cloneable {
	
	Point1 upperLeft, lowerRight; 

	public Rectangle(int x1, int y1, int x2, int y2 ) {
		
		upperLeft = new Point1(x1, y2);
		lowerRight = new Point1(x2, y2);
	}
	
	public void showPosition() {
		
		System.out.println("직사각형 위치정보...");
		System.out.println("좌 상단..");
		upperLeft.showPosition();
		System.out.println("");
		System.out.println("우 하단..");
		lowerRight.showPosition();
		System.out.println("\n");
		
	}
	
	public void changePos (int x1, int y1, int x2, int y2) {
		
		upperLeft.changePos(x1, y1);
		lowerRight.changePos(x2, y2);
	}

/*
    // shallowCopy
    // Rectangle 클래스의 인스턴스 변수 upperLeft와 lowerRight의 참조 값이 복사되었을 뿐, 
    // 참조 변수가 가리키는(참조하는) 인스턴스까지 복사가 된 것은 아니다.
      
  	@Override
	protected Object clone() throws CloneNotSupportedException {
		
		return super.clone();
	}
		
*/
	
	// DeepCopy 
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Rectangle copy = (Rectangle)super.clone();
		copy.upperLeft = (Point1)upperLeft.clone();
		copy.lowerRight = (Point1)lowerRight.clone();
				
		return copy;
	}
	
}

public class ShallowCopy {

	// DeepCopy와 ShallowCopy 동일 
	public static void main (String[] args) {
		
		Rectangle org = new Rectangle(1, 1, 9, 9);
		Rectangle cpy;
		
		try {
			
			cpy = (Rectangle)org.clone();
			org.changePos(2, 2, 7, 7);
			org.showPosition();
			cpy.showPosition();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
	}

}
