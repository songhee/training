package ObjectEquality;

class IntNumber {
	
	int num; 
	
	// 생성자는 별도의 반환형이 없다!!없다!!! 생성자는 반환형 없다!@@#!@# 
	public IntNumber (int num) {
		this.num = num;
	}

	public boolean isEquals (IntNumber numObj) {
		
		if (this.num == numObj.num) 
			return true;
		else 
			return false;
	}
}



public class ObjectEquality {
	
	public static void main(String[] args) {
		
		IntNumber num1 = new IntNumber(10);
		IntNumber num2 = new IntNumber(12);
		IntNumber num3 = new IntNumber(10);
		
		if (num1.isEquals(num2)) 
			System.out.println("num1과 num2는 동일한 정수");
		else 
			System.out.println("num1과 num2는 다른 정수");
		
		if (num1.isEquals(num3)) 
			System.out.println("num1과 num3은 동일한 정수");
		else 
			System.out.println("num1과 num3은 다은 정수");
		
	}
	
	
}
