package Singleton;

public class Main {

	public static void main(String[] args) {
		
		createSonghee();
	}
	
	public static void createSonghee() {
		God.getInstance().createObject();
	}
}


class God {
	private static God instance;
	
	private God() {
		
	}
	
	public static God getInstance() {
		if (instance == null) {
			instance = new God();
		} 
	
		return instance;
	}
	
	public void finalize() {
		instance = null;
	}
	
	public void createObject() {
		System.out.println("뚝딱~!");
	}
	
}