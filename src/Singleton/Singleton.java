package Singleton;

public class Singleton {
    private static Singleton uniqueInstance;
 
    // 기타 인스턴스 변수
    private Singleton() {
    }
 
    // synchronized 키워드만 추가하면 두 스레드가 이 메소드를 동시에 실행시키는 일은 일어나지 않게 된다.
    public static synchronized Singleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }
    // 기타 메소드
}
