package Thread;

class MessageSendingThread1 extends Thread {
	
	String message;
	
	public MessageSendingThread1(String str) {
		message = str;
	}
	
	public void run() {
		
		for (int i = 0; i < 1000000; i++) {
			
			System.out.println(message+"("+getPriority()+")");
		}
	}
}

public class PriorityTestOne {

	public static void main(String[] args) {
		MessageSendingThread1 tr1 = new MessageSendingThread1("First");
		MessageSendingThread1 tr2 = new MessageSendingThread1("Second");
		MessageSendingThread1 tr3 = new MessageSendingThread1("Third");
		tr1.start();
		tr2.start();
		tr3.start();    // 우선순위 동일 -> CPU할당을 나눠받음
		
	}
}