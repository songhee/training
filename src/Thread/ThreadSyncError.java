package Thread;

class Increment{

	int num=0;
	public synchronized void increment() {num++;}   // 'sychronize'라는 키워드를 통해 동기화 메소드가 됨 
	public int getNum() {return num;}
	
}

class incThread extends Thread {
	Increment inc; 
	
	public incThread(Increment inc) {
		
		this.inc = inc;
	}
	public void run() {
		for (int i = 0; i < 10000; i++) {
			for (int j = 0; j < 10000; j++) {
				inc.increment();
			}
		}
	}
}

public class ThreadSyncError {

	public static void main(String[] args) {
		Increment inc = new Increment();
		incThread it1 = new incThread(inc);
		incThread it2 = new incThread(inc);
		incThread it3 = new incThread(inc);
		
		it1.start();
		it2.start();
		it3.start();
		
		try {
			it1.join();
			it2.join();
			it3.join();
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		System.out.println(inc.getNum());
	}
}
