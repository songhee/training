package Thread;


class ShowThread extends Thread {    // 쓰레드를 상속해야 함 
	
	String threadName;
	
	public ShowThread(String name) {
		this.threadName = name;
	}
	
	public void run() {
		 
		for (int i = 0; i < 100; i++) {
			
			System.out.println("안녕하세요. "+threadName+"입니다.");
			try {
				
				sleep(100);   // delay (thread 클래스의 메소드)
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}


public class ThreadUnderstand {
	
	public static void main(String[] args) {
		
		ShowThread str1 = new ShowThread("이것이  쓰레드");
		ShowThread str2 = new ShowThread("예쁜 쓰레드");
		
		str1.start();   // 한 프로세스에 
		str2.start();   // 두 개의 프로그램이 실현됨 
	}

	 
}
