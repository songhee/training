package Wrapper;

public class UseWrapperClass {

	
	public static void showData(Object obj) {
		
		System.out.println(obj);
	}
	
	public static void main(String[] args) {
		
		Integer intInst = new Integer(3);
		showData(intInst);
		showData(new Integer(7));
		
	}
}


/*

Boolean			Boolean(boolean value)

Character       Character(char value)

Byte 			Byte(byte value)

Short 			Short(short value)

Integer 		Integer(int value)

Long			Long(log value)

Float 			Float(float value)

Double 			Double(double value)

*/
