package Wrapper;

class IntWrapper {
	
	private int num;
	public IntWrapper (int data) {
		
		num = data;	
	}
	
	public String toString() {
		
		return ""+num;  // 문자로 리턴함 
	}
	
}

public class WrappingInteger {

	public static void showDate(Object obj) {
		
		System.out.println(obj);
	}
	
	public static void main (String [] args) {
		
		IntWrapper intInst = new IntWrapper(3);
		showDate(intInst);
		showDate(new IntWrapper(7));
	}
}
