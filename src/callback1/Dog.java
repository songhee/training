package callback1;

public class Dog {
	
	private DogSound sound; 
	
	public Dog(DogSound sound) {
		this.sound = sound;
	}

	public void bark() {
		sound.bark();
	}
	
}
