package callback1;

public class Main {

	public static void main(String[] args) {
		
		DogSound mungmung = new DogSound() {

			public void bark() {
				System.out.println("mung!mung!");
				
			}
			
		};
		
		Poodle poodle = new Poodle(mungmung);
		Bulldog bulldog = new Bulldog(mungmung);
		Martis martis = new Martis(new DogSound() {
			
			@Override
			public void bark() {
				System.out.println("AngAng!");
				
			}
		}); 
		
		poodle.bark();
		bulldog.bark();
		martis.bark();

	}

}
