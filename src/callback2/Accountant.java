package callback2;

public class Accountant extends Employee {

	public Accountant(String name, String phone) {
		super(name, phone);
	}

	@Override
	public int getSalary() {
		return 5000000;
	}

	
}
