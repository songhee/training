package callback2;

import java.util.ArrayList;
import java.util.Iterator;

public class Company {

	// 멤버는 private로해라!! 
	private ArrayList<Employee> employList = new ArrayList<Employee>();
	
	// 생성자 
	
	// 사람을 고용하는 메소드
	public void addEmployee(Employee employee) {
		employList.add(employee);
	}
	
	// 사람을 해고하는 메소드 
	public void fireEmployee(Employee employee) {
		employList.remove(employee);
	}
	
	// 월급을 주는 메소드 
	public void printEmployeeSalary() {
		
		/*for (int i = 0; i < employList.size(); i++) {
			System.out.println(employList.get(i).getSalary());
		}*/
		
		Iterator iterator = employList.iterator();
		while (iterator.hasNext()) {    // boolean형 hasNext()메소드는 읽어올 요소가 남아 있는지 확인하는 메소드이다. 
			                            // 있으면 true를 반환하고 없으면 false를 반환한다. 
			Employee employee = (Employee) iterator.next();    // Objext형 next()메소드는 읽어올 요소가 남아있는지
			                                                   // 확인하는 메소드이다. 있으면 true, 없으면 false반환
			System.out.println(employee.getSalary());
		}
		
	}
	
	
	
	
	
	
	
}
