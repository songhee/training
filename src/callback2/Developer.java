package callback2;

public class Developer extends Employee {

	public Developer(String name, String phone) {
		super(name, phone);
	}

	@Override
	public int getSalary() {
		return 300000;
	}
	
}
