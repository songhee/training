package callback2;

public abstract class Employee {
	
	String name;
	String phone; 
	
	public Employee(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPhone() {
		return this.phone;
	}
	
	public abstract int getSalary();
	
}
