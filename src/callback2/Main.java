package callback2;

public class Main {
	
	public static void main(String[] args) {
		
		Company aksys = new Company();
		
		// 중요***
		aksys.addEmployee(new Developer("hsh", "0202324"));
		aksys.addEmployee(new Accountant("hjh", "0202342344"));
		aksys.addEmployee(new Salesman("kjyou", "23432324"));
		
		// 익명 클래스
		aksys.addEmployee(new Employee("hjyun", "010") {
			
			
			@Override
			public int getSalary() {
				return 50000000;
			}
		});
		
		aksys.printEmployeeSalary();
	}
	
}
