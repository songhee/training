package callback2;

public class Salesman extends Employee {

	public Salesman(String name, String phone) {
		super(name, phone);
	}

	@Override
	public int getSalary() {
		return 6000000;
	}

	
}
