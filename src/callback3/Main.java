package callback3;

public class Main {

	public static void main(String[] args) {
		
		Hjyun hjyun = new Hjyun();
		Main.showYourSalary(hjyun);
		
		Main.showYourSalary(new Employee() {
			
			@Override
			public int getSalary() {
				return 0;
			}
		});
		
	}
	
	public static void showYourSalary(Employee employee) {
		employee.getClass();
	}
}

interface Employee {
	public int getSalary();
}

class Hjyun implements Employee {
	public int getSalary() {
		return 400000;
	}
}
