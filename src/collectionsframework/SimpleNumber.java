package collectionsframework;

import java.util.HashSet;
import java.util.Iterator;

public class SimpleNumber {
	
	int num; 
	public SimpleNumber(int num) {
		this.num = num;
	}
	public String toString() {
		return String.valueOf(num);
	}
}


class HashSetEqulityOne {

	public static void main(String[] args) {
		HashSet<SimpleNumber> hSet = new HashSet<SimpleNumber>();
		hSet.add(new SimpleNumber(10));
		hSet.add(new SimpleNumber(20));
		hSet.add(new SimpleNumber(30));
		
		System.out.println("저장된 데이터 수 : "+hSet.size());
		
		Iterator<SimpleNumber> itr = hSet.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}